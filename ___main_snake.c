#define _POSIX_C_SOURCE 200112L

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
// Personal library
#include "interface_pack.h"
#include "snake_pack.h"
#include "map_pack.h"
#include "menu_pack.h"

__snake* snake1;
__snake* snake2;

// inicialize maps
__MAP GAME_MAPS[3] = { { .board = NULL, .length = 0}, 
					   { .board = &mapa_FEL, .length = 92},
					   { .board = &mapa_LAB, .length = 40}};
// setting default game options
__GAME GAME = {.x_food = 8, .y_food = 4, .speed = NORMAL, .game_on = false, 
			    .quit = false, .map_type = DEFAULT, .mode = SINGLE, .winner = NOBODY};

bool controls() {
	char c;
	if(read(STDIN_FILENO, &c, 1) == 1) {
		if(c == 'q') {
			GAME.quit = true;
			return false;
		}
		if(snake1->AI == USER) {
			switch (c) {
			case 'w':
				if (snake1->direction == RIGHT || snake1->direction == LEFT)
					snake1->direction = UP;
				break;
			case 'a':
				if(snake1->direction == UP || snake1->direction == DOWN)
					snake1->direction = LEFT;
				break;
			case 's':
				if(snake1->direction == LEFT || snake1->direction == RIGHT)
					snake1->direction = DOWN;
				break;
			case 'd':
				if(snake1->direction == UP || snake1->direction == DOWN)
					snake1->direction = RIGHT;
				break;
			default:
				break;
			}
		}
	}
	return true;
}


int main() {
	call_termios(0);
	MAIN_INTERFACE.mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
	MAIN_INTERFACE.parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (MAIN_INTERFACE.mem_base == NULL || MAIN_INTERFACE.parlcd_mem_base == NULL)
		exit(1);
	parlcd_hx8357_init(MAIN_INTERFACE.parlcd_mem_base);
	blinking();
	
    // ---------------------------- End of preparing -----------------------------------    

    system ("/bin/stty raw");
	system("stty -g > ~/.stty-save");
	system("stty -icanon min 0 time 0");

    char menu_steps[3] = {'N', 'N', 'N'};

    while(true) {    // MAIN while for MENU and GAME
		snake1 = init_snake(USER);
		snake2 = NULL;

		while(!GAME.game_on && !GAME.quit) {
        	menu_print(MAIN);
			refresh_display();
			if (read(STDIN_FILENO, &menu_steps[0], 1) == 1) {
				switch (menu_steps[0]) {
				case '1':
                    if(GAME.mode != SINGLE && GAME.map_type != DEFAULT){
                        menu_print(WARNING);
						refresh_display();
                        blinking();
						blinking();
                    } else {
					    GAME.game_on = true;
                    }
					break;
				case '2':
					while (menu_steps[0] != 'N') {
						menu_print(OPTIONS);
						refresh_display();
						if (read(STDIN_FILENO, &menu_steps[1], 1) == 1) {
							switch (menu_steps[1]) {
							case '1':
								while (menu_steps[1] != 'N') {
									menu_print(MODE);
									refresh_display();
									if (read(STDIN_FILENO, &menu_steps[2], 1) == 1) {
										switch (menu_steps[2]) {
										case '1':
											GAME.mode = SINGLE;
											break;
										case '2':
											GAME.mode = YOU_VS_PC;
											break;
										case '3':
											GAME.mode = PC_VS_PC;
											break;
										case '4':
											GAME.mode = ONLY_PC;
											break;
										case 'b':
											menu_steps[1] = 'N';
											menu_steps[2] = 'N';
											break;
										default:
											break;
										}
									}
								}
								break;
							case '2':
								while (menu_steps[1] != 'N') {
									menu_print(LEVEL);
									refresh_display();
									if (read(STDIN_FILENO, &menu_steps[2], 1) == 1) {
										switch (menu_steps[2]) {
										case '1':
											GAME.speed = NORMAL;
											break;
										case '2':
											GAME.speed = HARD;
											break;
										case '3':
											GAME.speed = EXTRA_HARD;
											break;
										case 'b':
											menu_steps[1] = 'N';
											menu_steps[2] = 'N';
											break;
										default:
											break;
										}
									}
								}
								break;
							case '3':
								while (menu_steps[1] != 'N') {
									menu_print(COLOUR);
									refresh_display();
									if (read(STDIN_FILENO, &menu_steps[2], 1) == 1) {
										switch (menu_steps[2]) {
										case '1':
											snake1->colour = CYAN;
											break;
										case '2':
											snake1->colour = ORANGE;
											break;
										case '3':
											snake1->colour = TOXIC_PINK;
											break;
										case 'b':
											menu_steps[1] = 'N';
											menu_steps[2] = 'N';
											break;
										default:
											break;
										}
									}
								}
								break;
							case '4':
								while (menu_steps[1] != 'N') {
									menu_print(MAP);
									refresh_display();
									if (read(STDIN_FILENO, &menu_steps[2], 1) == 1) {
										switch (menu_steps[2]) {
										case '1':
											GAME.map_type = DEFAULT;
											break;
										case '2':
											GAME.map_type = FEL;
											break;
										case '3':
											GAME.map_type = LAB;
											break;
										case 'b':
											menu_steps[1] = 'N';
											menu_steps[2] = 'N';
											break;
										default:
											break;
										}
									}
								}
								break;
							case 'b':
								menu_steps[0] = 'N';
								menu_steps[1] = 'N';
								break;
							default:
								break;
							}
						}
					}
					break;
				case '3':
					GAME.quit = true;
					break;
				default:
					break;
				}
			}
        
		}
        
		if(GAME.quit)
            break;

		switch (GAME.mode) {
		case SINGLE:
			break;
		case ONLY_PC:
			snake1->AI = PC;
			break;
		case YOU_VS_PC:
			snake2 = init_snake(PC);
			break;
		case PC_VS_PC:
			snake1->AI = PC;
			snake2 = init_snake(PC);
			break;
		default:
			break;
		}

		GAME.winner = NOBODY;
		GAME.x_food = 8;
		GAME.y_food = 4;

        while(true) {// Ваил для ИГРЫ
			if(!controls())
				break;
			
			menu_print(GAMING);
			if(GAME.map_type != DEFAULT)
                print_map();

            *(volatile uint32_t*)(MAIN_INTERFACE.mem_base + 0x010) = 0;
			*(volatile uint32_t*)(MAIN_INTERFACE.mem_base + 0x014) = 0;
			*(volatile uint32_t *)(MAIN_INTERFACE.mem_base + SPILED_REG_LED_LINE_o) = 4026531855;

			if (GAME.mode != SINGLE && GAME.mode != ONLY_PC) {
				if (!do_multiplayer_move(snake1, snake2)) {
					break;
				}
			} else {
				if (!do_single_move(snake1)) {
					GAME.winner = PLAYER_2;
                	break;
				}
			} 
			
            refresh_display();
			usleep(GAME.speed);
        }

        if(GAME.quit)
            break;

		menu_print(END_GAMING);
		refresh_display();
		blinking();
        GAME.game_on = false;
		switch (GAME.mode) {
			case SINGLE:
			case ONLY_PC:
				delete_snake(snake1);
				break;
			case YOU_VS_PC:
			case PC_VS_PC:
				delete_snake(snake1);
				delete_snake(snake2);
				break;
			default:
				break;
		}
    }

	menu_print(END_PROGRAM);
	*(volatile uint32_t*)(MAIN_INTERFACE.mem_base + 0x010) = 0;
	*(volatile uint32_t*)(MAIN_INTERFACE.mem_base + 0x014) = 0;
	*(volatile uint32_t *)(MAIN_INTERFACE.mem_base + SPILED_REG_LED_LINE_o) = 0;
	refresh_display();

	system("stty $(cat ~/.stty-save)");
	call_termios(1);
	return 0;
}

