// #include <stdint.h>
#include "interface_pack.h"
#include "mzapo_regs.h"
#include <termios.h>
#include <unistd.h>

void blinking() {
	uint32_t fwrd_line = 15;
	uint32_t back_line = 4026531840;
	for (int i = 0; i <= 28; i++) {
		if(i > 14){
			*(volatile uint32_t*)(MAIN_INTERFACE.mem_base + 0x010) = 0x00ffff00;
			*(volatile uint32_t*)(MAIN_INTERFACE.mem_base + 0x014) = 0;
		}else {
			*(volatile uint32_t*)(MAIN_INTERFACE.mem_base + 0x010) = 0;
			*(volatile uint32_t*)(MAIN_INTERFACE.mem_base + 0x014) = 0x00ffff00;
		}
		if(i >= 12 && i <= 14){
			*(volatile uint32_t *)(MAIN_INTERFACE.mem_base + SPILED_REG_LED_LINE_o) = 245760;
		}else {
			*(volatile uint32_t *)(MAIN_INTERFACE.mem_base + SPILED_REG_LED_LINE_o) = fwrd_line + back_line; // тут тупо скорее подсветка 32 желтых диодов
		}
		fwrd_line <<= 1;
		back_line >>= 1;
		usleep(40000);
	}
	*(volatile uint32_t*)(MAIN_INTERFACE.mem_base + 0x010) = 0;
	*(volatile uint32_t*)(MAIN_INTERFACE.mem_base + 0x014) = 0;
}

void refresh_display() {
	parlcd_write_cmd(MAIN_INTERFACE.parlcd_mem_base, 0x2c);
	for (int y = 0; y < 320; y++){
		for (int x = 0; x < 480; x++){
			parlcd_write_data(MAIN_INTERFACE.parlcd_mem_base, mydisplay[y][x]);
		}
	}
}

void call_termios(int reset) {
    static struct termios tio, tioOld;
    tcgetattr(STDIN_FILENO, &tio);
    if (reset) {
        tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
    } else {
        tioOld = tio; //backup 
        cfmakeraw(&tio);
        tio.c_oflag |= OPOST;
        tcsetattr(STDIN_FILENO, TCSANOW, &tio);
    }
}
