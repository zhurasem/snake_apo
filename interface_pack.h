#ifndef __INTERFACE_PACK__
#define __INTERFACE_PACK__
#include <stdint.h>

#define LARGE_FONT_HEIGHT  4
#define NORMAL_FONT_HEIGHT 3
#define SMALL_FONT_HEIGHT  2

#define PIXEL_SIZE 20
#define LINE_SIZE 10

// __interface - structure storing pointers to the periphery
typedef struct {
	unsigned char *mem_base;		// 2 large diodes and a line of 32 diodes
	unsigned char *parlcd_mem_base;	// display
} __interface;

__interface MAIN_INTERFACE;
enum colour_enum    {PURPLE = 0x901f, ORANGE = 0xfc40, LIGHT_GREEN = 0x07e0, TOXIC_PINK = 0xf817, INTERFACE_COLOR = 0xbf26, 
					WHITE = 0xffff, LIGHT_GRASS = 0x0e00, DARK_GRASS = 0x2d42, BACKGROUND_GREEN = 0x2d42, CYAN = 0x07bf};
uint16_t mydisplay[320][480];

void blinking();
void refresh_display();	// Копируем значения из буфера в буфер lcd
void call_termios(int reset); // work with terminal

#endif