#ifndef __MAP_PACK__
#define __MAP_PACK__
#include <stdio.h>
#include "interface_pack.h"
#include "snake_pack.h"

// __MAP - map structure
typedef struct {
	unsigned char* board;// map name
	int length;		     // number of blocks
} __MAP;

enum map_enum 		{DEFAULT, FEL, LAB};

static unsigned char mapa_FEL[] = {2,2, 3,2, 4,2, 5,2, 6,2, 2,3, 2,4, 2,5, 2,6, 2,7, 2,8, 2,9, 3,5, 4,5,
								 	5,5, 9,2, 10,2, 11,2, 12,2, 13,2, 9,3, 9,4, 9,5, 9,6, 9,7, 9,8, 9,9, 
									10,5, 11,5, 12,5, 10,9, 11,9, 12,9, 13,9, 16,2, 16,3, 16,4, 16,5, 16,6, 
									16,7, 16,8, 16,9, 17,9, 18,9, 19,9, 20,9};

static unsigned char mapa_LAB[] = {11,0, 11,1, 11,2, 11,3, 11,8, 11,9, 11,10, 11,11, 5,3, 5,4, 5,5, 5,6, 5,7,
									5,8, 17,3, 17,4, 17,5, 17,6, 17,7, 17,8};

extern __MAP GAME_MAPS[3];


#endif