#include <string.h>
#include "menu_pack.h"
#include "snake_pack.h"

void menu_print(int mode) {

	if(mode == END_PROGRAM) {
		for(int i = 0; i < 320; i++) {
			for(int j = 0; j < 480; j++) {
				mydisplay[i][j] = 0;
			}
		}
		return;
	}

    for(int i = 0; i < 320 ; i++){
        for(int j = 0; j < 480 ; j++){
			if(i < LINE_SIZE || (i >= 320 - LINE_SIZE)) {
				mydisplay[i][j] = INTERFACE_COLOR; // рисует линию вверху и внизу над текстом ВЕЗДЕ
			} else if((i >= 90 && i <= 100) && mode != WARNING && mode != GAMING && mode != END_GAMING) {
				mydisplay[i][j] = INTERFACE_COLOR; // горизонтальная черта отделяет название МЕНЮ
			} else if((j < LINE_SIZE) || (j >= 480 - LINE_SIZE)) {
				mydisplay[i][j] = INTERFACE_COLOR; // рисует линию по бокам ВЕЗДЕ
			} else if(mode == GAMING && i > 260 && j >= 235 && j <= 245) {
				mydisplay[i][j] = INTERFACE_COLOR; // вертикальная черта между очками соперников ИГРА
			} else if(mode == GAMING && (i < 260 && i >= 260 - LINE_SIZE)) {
				mydisplay[i][j] = INTERFACE_COLOR; // верикаальная черта вверху разделяющая очки от поля ИГРА
			} else if(mode == GAMING && i - 5 >= GAME.x_food*PIXEL_SIZE + LINE_SIZE && i + 5 < GAME.x_food*PIXEL_SIZE + PIXEL_SIZE + LINE_SIZE &&
					 j - 5 >= GAME.y_food*PIXEL_SIZE + LINE_SIZE && j + 5< GAME.y_food*PIXEL_SIZE + PIXEL_SIZE + LINE_SIZE){
                mydisplay[i][j] = WHITE; // хавчик ИГРА
			}else {
				if(mode == GAMING && i >= LINE_SIZE && i <= 260 && j >= LINE_SIZE && j < 480 - LINE_SIZE) {
					if((i/10) % 2 == 0)
						mydisplay[i][j] = LIGHT_GRASS; // all other
					else
						mydisplay[i][j] = DARK_GRASS;
				} else 
					mydisplay[i][j] = BACKGROUND_GREEN; // все оставшееся
			}
        }
    }
    switch (mode) {
    case MAIN:     // Main menu mode
		print_text(Main1, WHITE, LARGE_FONT_HEIGHT, strlen(Main1), 50, 20);
		print_text(Main2, WHITE, SMALL_FONT_HEIGHT, strlen(Main2), 20, 100);
		print_text(Main3, WHITE, SMALL_FONT_HEIGHT, strlen(Main3), 20, 145);
		print_text(Main4, WHITE, SMALL_FONT_HEIGHT, strlen(Main4), 20, 190);
        break;

	case WARNING:
		print_text(Warn1, WHITE, NORMAL_FONT_HEIGHT, strlen(Warn1), 70, 50);
		print_text(Warn2, WHITE, NORMAL_FONT_HEIGHT, strlen(Warn2), 50, 130);
		print_text(Warn3, WHITE, NORMAL_FONT_HEIGHT, strlen(Warn3), 30, 210);
		break;

	case GAMING:
		if(snake1->AI == USER){
			char Scor1[] = "YOU:";
			sprintf(&Scor1[4], "%d", snake1->length);
			print_text(&Scor1, snake1->colour, NORMAL_FONT_HEIGHT, strlen(Scor1), 40, 260);
		} else {
			char Scor1[] = "BOT:";
			sprintf(&Scor1[4], "%d", snake1->length);
			print_text(&Scor1, WHITE, NORMAL_FONT_HEIGHT, strlen(Scor1), 40, 260);
		}
		if(GAME.mode != SINGLE && GAME.mode != ONLY_PC) {
			char Scor2[] = "BOT:";
			sprintf(&Scor2[4], "%d", snake2->length);
			print_text(&Scor2, WHITE, NORMAL_FONT_HEIGHT, strlen(Scor2), 270, 260);
		}
		break;
	
	case END_GAMING:
		if(GAME.mode != SINGLE && GAME.mode != ONLY_PC) {
			char End1[] = "The Best";
			char End2[] = "Player is  ";
			char End3[] = "Score:    ";
			if(GAME.winner == PLAYER_1 && snake1->AI == USER){
				sprintf(&End2[10], "YOU");
			} else {
				sprintf(&End2[10], "BOT");
			}
			if(GAME.winner == PLAYER_2){
				sprintf(&End2[10], "BOT");
			}
			//sprintf(&End2[10], "%d", GAME.winner);
			print_text(&End1, WHITE, LARGE_FONT_HEIGHT, strlen(End1), 90, 50);
			print_text(&End2, WHITE, LARGE_FONT_HEIGHT, strlen(End2), 30, 130);

			switch(GAME.winner) {
				case PLAYER_1:
					sprintf(&End3[8], "%d", snake1->length);
					break;
				case PLAYER_2:
					sprintf(&End3[8], "%d", snake2->length);
					break;
			}
			print_text(End3, WHITE, LARGE_FONT_HEIGHT, strlen(End3), 100, 210);
		} else {
			char *End4 = "Your score";
			char End5[] = "is :   ";
			sprintf(&End5[6], "%d", snake1->length);
			print_text(End4, WHITE, LARGE_FONT_HEIGHT, strlen(End4), 60, 80);
			print_text(&End5, WHITE, LARGE_FONT_HEIGHT, strlen(End5), 140, 160);
		}
		break;
	
	
	case OPTIONS:		// Options menu mode
		print_text(Opti1, WHITE, LARGE_FONT_HEIGHT, strlen(Opti1), 50, 20);
		print_text(Opti2, WHITE, SMALL_FONT_HEIGHT, strlen(Opti2), 20, 100);
		print_text(Opti3, WHITE, SMALL_FONT_HEIGHT, strlen(Opti3), 20, 145);
		print_text(Opti4, WHITE, SMALL_FONT_HEIGHT, strlen(Opti4), 20, 190);
		print_text(Opti5, WHITE, SMALL_FONT_HEIGHT, strlen(Opti5), 20, 235);
		print_text(Back1, WHITE, SMALL_FONT_HEIGHT, strlen(Back1), 20, 280);
		break;

	case MODE:		// Mode menu mode
        print_text(Mode1, WHITE, LARGE_FONT_HEIGHT, strlen(Mode1), 50, 20);		// MODE
		if(GAME.mode == SINGLE)													// 1. SINGLE
			print_text(Mode2, LIGHT_GREEN, SMALL_FONT_HEIGHT, strlen(Mode2), 20, 100);
		else
			print_text(Mode2, WHITE, SMALL_FONT_HEIGHT, strlen(Mode2), 20, 100);
		if(GAME.mode == YOU_VS_PC)												// 2. YOU VS PC
			print_text(Mode3, LIGHT_GREEN, SMALL_FONT_HEIGHT, strlen(Mode3), 20, 145);
		else
			print_text(Mode3, WHITE, SMALL_FONT_HEIGHT, strlen(Mode3), 20, 145);
		if(GAME.mode == PC_VS_PC)
			print_text(Mode4, LIGHT_GREEN, SMALL_FONT_HEIGHT, strlen(Mode4), 20, 190);		
		else
			print_text(Mode4, WHITE, SMALL_FONT_HEIGHT, strlen(Mode4), 20, 190);
		if(GAME.mode == ONLY_PC)												// 4. ONLY PC
			print_text(Mode5, LIGHT_GREEN, SMALL_FONT_HEIGHT, strlen(Mode5), 20, 235);
		else
			print_text(Mode5, WHITE, SMALL_FONT_HEIGHT, strlen(Mode5), 20, 235);
		print_text(Back2, WHITE, SMALL_FONT_HEIGHT, strlen(Back2), 20, 280);
		break;
	
	case LEVEL:		// Level menu mode
		
        print_text(Levl1, WHITE, LARGE_FONT_HEIGHT, strlen(Levl1), 50, 20);		 // LEVEL
		if(GAME.speed == NORMAL)												 // 1. NORMAL
			print_text(Levl2, LIGHT_GREEN, SMALL_FONT_HEIGHT, strlen(Levl2), 20, 100);
		else
			print_text(Levl2, WHITE, SMALL_FONT_HEIGHT, strlen(Levl2), 20, 100);
		if(GAME.speed == HARD)													 // 2. HARD
			print_text(Levl3, LIGHT_GREEN, SMALL_FONT_HEIGHT, strlen(Levl3), 20, 145);
		else
			print_text(Levl3, WHITE, SMALL_FONT_HEIGHT, strlen(Levl3), 20, 145);
		if(GAME.speed == EXTRA_HARD)											 // 3. EXTRA HARD
			print_text(Levl4, LIGHT_GREEN, SMALL_FONT_HEIGHT, strlen(Levl4), 20, 190);
		else
			print_text(Levl4, WHITE, SMALL_FONT_HEIGHT, strlen(Levl4), 20, 190);
		print_text(Back2, WHITE, SMALL_FONT_HEIGHT, strlen(Back2), 20, 235);
		break;

	case COLOUR:		// Colour menu mode
		print_text(Colr1, WHITE, LARGE_FONT_HEIGHT, strlen(Colr1), 50, 20);
		if(snake1->colour == CYAN)
			print_text(Colr2, LIGHT_GREEN, SMALL_FONT_HEIGHT, strlen(Colr2), 20, 100);
		else
			print_text(Colr2, WHITE, SMALL_FONT_HEIGHT, strlen(Colr2), 20, 100);
		if(snake1->colour == ORANGE)
			print_text(Colr3, LIGHT_GREEN, SMALL_FONT_HEIGHT, strlen(Colr3), 20, 145);
		else
			print_text(Colr3, WHITE, SMALL_FONT_HEIGHT, strlen(Colr3), 20, 145);
		if(snake1->colour == TOXIC_PINK)
			print_text(Colr4, LIGHT_GREEN, SMALL_FONT_HEIGHT, strlen(Colr4), 20, 190);
		else
			print_text(Colr4, WHITE, SMALL_FONT_HEIGHT, strlen(Colr4), 20, 190);
		print_text(Back2, WHITE, SMALL_FONT_HEIGHT, strlen(Back2), 20, 235);
		break;

    case MAP:
        print_text(Mapa1, WHITE, LARGE_FONT_HEIGHT, strlen(Mapa1), 50, 20);		 // MAP
		if(GAME.map_type == DEFAULT)												 	// 1. DEFAULT
			print_text(Mapa2, LIGHT_GREEN, SMALL_FONT_HEIGHT, strlen(Mapa2), 20, 100);
		else
			print_text(Mapa2, WHITE, SMALL_FONT_HEIGHT, strlen(Mapa2), 20, 100);
		if(GAME.map_type == FEL)														 // 2. FEL
			print_text(Mapa3, LIGHT_GREEN, SMALL_FONT_HEIGHT, strlen(Mapa3), 20, 145);
		else
			print_text(Mapa3, WHITE, SMALL_FONT_HEIGHT, strlen(Mapa3), 20, 145);
		if(GAME.map_type == LAB)														 // 3. LAB
			print_text(Mapa4, LIGHT_GREEN, SMALL_FONT_HEIGHT, strlen(Mapa4), 20, 190);
		else
			print_text(Mapa4, WHITE, SMALL_FONT_HEIGHT, strlen(Mapa4), 20, 190);
		print_text(Back2, WHITE, SMALL_FONT_HEIGHT, strlen(Back2), 20, 235);
        break;
    default:
        break;
    }
}

void print_text(char *text, int colour, int font, int length, int x, int y) {
	int space = font == NORMAL_FONT_HEIGHT ? 3 : 5;
    for(int lttr = 0; lttr < length; lttr++){
        if((*text >= font_dscr->firstchar) && (*text - font_dscr->firstchar < font_dscr->size)){
            uint16_t *letter =  ((*text - font_dscr->firstchar) * font_dscr->height) + font_dscr->bits;
            int width = font_dscr->width[*text - font_dscr->firstchar];
			int height = font_dscr->height;
            for (int i = 0; i < height; i++){
                uint16_t line = *(letter++);
                for (int j = 0; j < width; j++){
                    if ((line & 0x8000) != 0)
						print_pixel(x+j*font, y+i*font, font, colour);
                    line <<= 1;
                }
            }
        }
        x += font_dscr->width[*(text++) - font_dscr->firstchar] * font + space;
    }
}

void print_pixel(int x, int y, int size, int colour) {   // OK draw_scale
    for(int j = 0; j < size; j++){
        for(int i = 0; i < size; i++){
            mydisplay[y+j][x+i] = colour;
        }
    }
}
