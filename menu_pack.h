#ifndef __MENU_PACK__
#define __MENU_PACK__
#include "interface_pack.h"
#include "font_types.h"

static font_descriptor_t* font_dscr = &font_winFreeSystem14x16;
enum menu_enum 		{MAIN, OPTIONS, MODE, LEVEL, COLOUR, MAP, WARNING, GAMING, END_GAMING, END_PROGRAM};

// Main menu
static char *Main1 = "Snake";
static char *Main2 = "1. Play";
static char *Main3 = "2. Options";
static char *Main4 = "3. Exit";

// Options menu
static char *Opti1 = "Options";
static char *Opti2 = "1. Mode";
static char *Opti3 = "2. Level";
static char *Opti4 = "3. Colour";
static char *Opti5 = "4. Map";

// Mode menu
static char *Mode1 = "Mode";
static char *Mode2 = "1. Single";
static char *Mode3 = "2. You VS PC";
static char *Mode4 = "3. PC VS PC";
static char *Mode5 = "4. Only PC";

// Level menu
static char *Levl1 = "Level";
static char *Levl2 = "1. Normal";
static char *Levl3 = "2. Hard";
static char *Levl4 = "3. Extra hard";

// Colour menu
static char *Colr1 = "Colour";
static char *Colr2 = "1. Cyan";
static char *Colr3 = "2. Orange";
static char *Colr4 = "3. Pink";

// Map menu
static char *Mapa1 = "Map";
static char *Mapa2 = "1. Default";
static char *Mapa3 = "2. FEL";
static char *Mapa4 = "3. Labyrinth";

// Warning window
static char *Warn1 = "Other maps can";
static char *Warn2 = "only be played in";
static char *Warn3 = "single player mode";

// Back button
static char *Back1 = "B - Back to menu";
static char *Back2 = "B - Back to options";

void menu_print(int mode);
void print_pixel(int x, int y, int size, int colour);
void print_text(char *text, int colour, int font, int length, int x, int y);

#endif