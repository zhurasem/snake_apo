#include "snake_pack.h"
#include "mzapo_regs.h"
#include <stdlib.h>

__snake* init_snake(int user_type) {

	__cell *new_head = malloc(sizeof(__cell));
    new_head->x_position = user_type ? 10 : 1; // PC spawn on 10 and USER spawn on 1
    new_head->y_position = user_type ? 21 : 1; // PC spawn on 21 and USER spawn on 1
    new_head->next_cell = NULL;
    __snake *new_snake = malloc(sizeof(__snake));
    new_snake->head = new_head;
    new_snake->length = 3;
    new_snake->colour = user_type ? PURPLE : CYAN; // PC have 0x001f color and USER have 0xf800 color
    new_snake->AI = user_type;
    new_snake->direction = user_type ? UP : DOWN; // PC move to the UP and USER move to the DOWN
	return new_snake;
}

void delete_snake(__snake* deleting_snake) {

	__cell* visible_element = deleting_snake->head;
	__cell* temporary = NULL;
	for (int part_snake = 0; part_snake < deleting_snake->length; part_snake++) {
		if (visible_element->next_cell != NULL) {
			temporary = visible_element;
			visible_element = visible_element->next_cell;
			free(temporary);
		}
	}
	free(visible_element);
	free(deleting_snake);
}

void move_snake(__snake *snake){
	__cell *cell = malloc(sizeof(__cell));
	switch (snake->direction) {
	case DOWN:
		cell->x_position = (snake->head)->x_position + 1;
		cell->y_position = (snake->head)->y_position;
		break;
	case LEFT:
		cell->x_position = (snake->head)->x_position;
		cell->y_position = (snake->head)->y_position - 1;
		break;
	case RIGHT:
		cell->x_position = (snake->head)->x_position;
		cell->y_position = (snake->head)->y_position + 1;
		break;
	case UP:
		cell->x_position = (snake->head)->x_position - 1;
		cell->y_position = (snake->head)->y_position;
		break;
	default:
		break;
	}
	cell->next_cell = snake->head;
	snake->head = cell;

	// Redraw snake in virtual desk
	int counter = 1;
	int dis_c_x; // display coordinate x
	int dis_c_y; // display coordinate y
	__cell *temporary = snake->head;
	while(temporary != NULL) {
		for(int c_x = 3; c_x < PIXEL_SIZE - 3; c_x++) {	// coordinate x
			for(int c_y = 3; c_y < PIXEL_SIZE - 3; c_y++) { // coordinate y
				dis_c_x = temporary->x_position* PIXEL_SIZE + c_x + LINE_SIZE;
				dis_c_y = temporary->y_position* PIXEL_SIZE + c_y + LINE_SIZE;
				mydisplay[dis_c_x][dis_c_y] = snake->colour;
			}
		}

		if(counter == snake->length) {
			free(temporary->next_cell);
			temporary->next_cell = NULL;
			break;
		}
		counter++;
		temporary = temporary->next_cell;
	}
}

bool check_collision(__snake *snake) {

	if(snake->head->x_position >= HEIGHT_BOARD|| snake->head->y_position >= WIDTH_BOARD 
	   || snake->head->x_position < 0 || snake->head->y_position < 0) {
		return true;
	}
	__cell* temporary = snake->head->next_cell;
	int counter = 1;
	while(temporary != NULL) {
		if(snake->head->x_position == temporary->x_position && 
			snake->head->y_position == temporary->y_position) {
			return true;
		}
		if(counter == snake->length) {
			break;
		}
		temporary = temporary->next_cell;
		counter++;
	}
	return false;
}

bool check_collision_map(__cell *tile) {

	int length_map = GAME_MAPS[GAME.map_type].length;
	__MAP* selected_map = &GAME_MAPS[GAME.map_type].board;
	
	for(int element = 0; element < length_map; element += 2) {
		
		if(tile->x_position == selected_map->board[element + 1] && 
		   tile->y_position == selected_map->board[element])
			return true;
		}
	return false;
}

bool check_collision_multiplayer(__snake* snake, __snake* enemy) {

	__cell* temporary = enemy->head;
	int counter = 1;
	while(temporary != NULL) {
		if(snake->head->x_position == temporary->x_position && 
			snake->head->y_position == temporary->y_position) {
			return true;
		}
		if(counter == enemy->length) {
			break;
		}
		temporary = temporary->next_cell;
		counter++;
	}

	return false;
}

int get_new_direction(int present_direction, int turn_to) {

	switch (present_direction) {
	case UP:
		if (turn_to == TURN_RIGHT)
			return RIGHT;
		else 
			return LEFT;
	case RIGHT:
		if (turn_to == TURN_RIGHT)
			return DOWN;
		else
			return UP;
	case LEFT:
		if (turn_to == TURN_RIGHT)
			return UP;
		else
			return DOWN;
	case DOWN:
		if (turn_to == TURN_RIGHT)
			return LEFT;
		else
			return RIGHT;
	}
}

void spawn_food(__snake *snake) {

	if (GAME.x_food == (snake->head)->x_position && GAME.y_food == (snake->head)->y_position) { //blue led - eaten food
		if(snake->AI == USER) {
			*(volatile uint32_t*)(MAIN_INTERFACE.mem_base + 0x010) = 0x0000cc00;
			*(volatile uint32_t*)(MAIN_INTERFACE.mem_base + 0x014) = 0x0000cc00;
		} else {
			*(volatile uint32_t*)(MAIN_INTERFACE.mem_base + 0x010) = 0x00ff0000;
			*(volatile uint32_t*)(MAIN_INTERFACE.mem_base + 0x014) = 0x00ff0000;
		}
		*(volatile uint32_t *)(MAIN_INTERFACE.mem_base + SPILED_REG_LED_LINE_o) = 4294967295;
		bool main_while = true, intersection_food = false;
		__cell* buffer_cell;
		while(main_while) {
			main_while = true;
			intersection_food = false;
			buffer_cell = snake->head;
			GAME.x_food = rand() % ((260 - 2 * LINE_SIZE) / PIXEL_SIZE);
			GAME.y_food = rand() % ((480 - 2 * LINE_SIZE) / PIXEL_SIZE);

			for(int part_snake = 0; part_snake < snake->length; part_snake++) { // Checking spawn food in snake
				if(buffer_cell->x_position == GAME.x_food && buffer_cell->y_position == GAME.y_food){
					intersection_food = true;
					break;
				}
				buffer_cell = buffer_cell->next_cell;
			}
			
			if(intersection_food) { // Next generation if food was spawned into snake
				intersection_food = false;
				continue;
			}

			if (GAME.map_type != DEFAULT) {
				int max_length = GAME_MAPS[GAME.map_type].length;
				__MAP* game_map = &GAME_MAPS[GAME.map_type].board;
				for (int counter = 0; counter < max_length; counter++) {
					if(GAME.x_food == game_map->board[counter + 1] && GAME.y_food == game_map->board[counter]) {
						intersection_food = true;
						break;
					}
				}
			}

			if(intersection_food) {	// Start new while if food was spawned into a map
				intersection_food = false;
			} else {
				main_while = false;
			}
		}
		snake->length += 1;
	}
}

bool do_single_move(__snake* snake) {
	if (snake->AI == PC) {
		select_new_direction_BOT(snake, NULL);
	}
	move_snake(snake);
	spawn_food(snake);

	if(GAME.map_type != DEFAULT) {
		if(check_collision_map(snake->head)) {
			return false;
		}
	}
	if(check_collision(snake)) {
		return false;
	}
	return true;
}

bool do_multiplayer_move(__snake* player_1, __snake* player_2) {
	// first player's step
	if (player_1->AI == PC) {
		select_new_direction_BOT(player_1, player_2);
	}
	move_snake(player_1);
	spawn_food(player_1);

	if(check_collision(player_1) || check_collision_multiplayer(player_1, player_2)) {
		GAME.winner = PLAYER_2;
		return false;
	}

	// second player's step
	select_new_direction_BOT(player_2, player_1);
	move_snake(player_2);
	spawn_food(player_2);

	if(check_collision(player_2) || check_collision_multiplayer(player_2, player_1)) {
		GAME.winner = PLAYER_1;
		return false;
	}

	return true;
}

void select_new_direction_BOT(__snake *snake, __snake *enemy) {
	bool direction_changed = false;
	if (snake->AI == true) {
		int way_to_x = GAME.x_food - (snake->head)->x_position; // define where is food (coordinate x)
		int way_to_y = GAME.y_food - (snake->head)->y_position; // define where is food (coordinate y)
		if (way_to_y < 0) {
			if (snake->direction == LEFT) {
				direction_changed = false; // do nothing
			} else if (snake->direction == DOWN) {
				if (!try_possible_move(snake, TURN_RIGHT, enemy)) {
					snake->direction = get_new_direction(snake->direction, TURN_RIGHT);
					direction_changed = true;
				}
			} else if (snake->direction == UP) {
				if (!try_possible_move(snake, TURN_LEFT, enemy)) {
					snake->direction = get_new_direction(snake->direction, TURN_LEFT);
					direction_changed = true;
				}
			} else if (snake->direction == RIGHT) {
				if (way_to_x > 0) {
					if (!try_possible_move(snake, TURN_RIGHT, enemy)) {
						snake->direction = get_new_direction(snake->direction, TURN_RIGHT);
						direction_changed = true;
					}
				} else {
					if (!try_possible_move(snake, TURN_LEFT, enemy)) {
						snake->direction = get_new_direction(snake->direction, TURN_LEFT);
						direction_changed = true;
					}
				}
			}
		}
		if (way_to_x < 0 && direction_changed == false) {
			if (snake->direction == UP) {
				direction_changed = false; // do nothing
			} else if (snake->direction == LEFT) {
				if (!try_possible_move(snake, TURN_RIGHT, enemy)) {
					snake->direction = get_new_direction(snake->direction, TURN_RIGHT);
					direction_changed = true;
				}
			} else if (snake->direction == RIGHT) {
				if (!try_possible_move(snake, TURN_LEFT, enemy)) {
					snake->direction = get_new_direction(snake->direction, TURN_LEFT);
					direction_changed = true;
				}
			} else if (snake->direction == DOWN) {
				if (way_to_y > 0) {
					if (!try_possible_move(snake, TURN_LEFT, enemy)) {
						snake->direction = get_new_direction(snake->direction, TURN_LEFT);
						direction_changed = true;
					}
				} else {
					if (!try_possible_move(snake, TURN_RIGHT, enemy)) {
						snake->direction = get_new_direction(snake->direction, TURN_RIGHT);
						direction_changed = true;
					}
				}
			}
		}
		if (way_to_y > 0 && direction_changed == false) {
			if (snake->direction == RIGHT) {
				direction_changed = false; // do nothing
			} else if (snake->direction == UP) {
				if (!try_possible_move(snake, TURN_RIGHT, enemy)) {
					snake->direction = get_new_direction(snake->direction, TURN_RIGHT);
					direction_changed = true;
				}
			} else if (snake->direction == DOWN) {
				if (!try_possible_move(snake, TURN_LEFT, enemy)) {
					snake->direction = get_new_direction(snake->direction, TURN_LEFT);
					direction_changed = true;
				}
			} else if (snake->direction == LEFT) {
				if (way_to_x > 0) {
					if (!try_possible_move(snake, TURN_LEFT, enemy)) {
						snake->direction = get_new_direction(snake->direction, TURN_LEFT);
						direction_changed = true;
					}
				} else {
					if (!try_possible_move(snake, TURN_RIGHT, enemy)) {
						snake->direction  = get_new_direction(snake->direction, TURN_RIGHT);
						direction_changed = true;
					}
				}
			}
		}
		if (way_to_x > 0 && direction_changed == false) {
			if (snake->direction == DOWN) {
				direction_changed = false; // do nothing
			} else if (snake->direction == LEFT) {
				if (!try_possible_move(snake, TURN_LEFT, enemy)) {
					snake->direction = get_new_direction(snake->direction, TURN_LEFT);
					direction_changed = true;
				}
			} else if (snake->direction == RIGHT) {
				if (!try_possible_move(snake, TURN_RIGHT, enemy)) {
					snake->direction  = get_new_direction(snake->direction, TURN_RIGHT);
					direction_changed = true;
				}
			} else if (snake->direction == DOWN) {
				if (way_to_y > 0) {
					if (!try_possible_move(snake, TURN_LEFT, enemy)) {
						snake->direction = get_new_direction(snake->direction, TURN_LEFT);
						direction_changed = true;
					}
				} else {
					if (!try_possible_move(snake, TURN_RIGHT, enemy)) {
						snake->direction = get_new_direction(snake->direction, TURN_RIGHT);
						direction_changed = true;
					}
				}
			}
		}
	}
}
// check possible positions to move on the way
bool try_possible_move(__snake *snake, int change_in_direction, __snake *enemy) {
	int x_possible_move = 0;
	int y_possible_move = 0;
	
	switch (get_new_direction(snake->direction, change_in_direction)) {
	case RIGHT:
		x_possible_move = 0;
		y_possible_move = 1;
		break;
	case DOWN:
		x_possible_move = 1;
		y_possible_move = 0;
		break;
	case LEFT:
		x_possible_move = 0;
		y_possible_move = -1;
		break;
	case UP:
		x_possible_move = -1;
		y_possible_move = 0;
		break;
	}
	int new_x = (snake->head)->x_position + x_possible_move;
	int new_y = (snake->head)->y_position + y_possible_move;
	__cell *temporary = snake->head;
	while (temporary != NULL) { // collision with yourself
		if (new_x == temporary->x_position && temporary->y_position == new_y) {
			return true;
		}
		temporary = temporary->next_cell;
	}
	if(GAME.mode != SINGLE && GAME.mode != ONLY_PC) {
		if(check_collision_multiplayer(snake, enemy)) return true;
	}
	return false;
}

void print_map() {		// Рисует mapa

	int counter = 0;
	int c_dis_x; // x coordinate of the display item
	int c_dis_y; // y coordinate of the display item
	while(counter < GAME_MAPS[GAME.map_type].length) {
		for (int c_x = 0; c_x < PIXEL_SIZE; c_x++) { // c_x - element's coordinate x
			for (int c_y = 0; c_y < PIXEL_SIZE; c_y++) { // c_y - element's coordinate x
				c_dis_x = GAME_MAPS[GAME.map_type].board[counter + 1]* PIXEL_SIZE + c_x + LINE_SIZE;
				c_dis_y = GAME_MAPS[GAME.map_type].board[counter]* PIXEL_SIZE+ c_y + LINE_SIZE;
				mydisplay[c_dis_x][c_dis_y] = INTERFACE_COLOR;	
			}
		}
		counter += 2;
	}
}
