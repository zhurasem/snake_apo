#ifndef __SNAKE_PACK__
#define __SNAKE_PACK__
#include <stdbool.h>
#include "interface_pack.h"
#include "map_pack.h"

// __cell - structure of one cell of the snake
typedef struct {
    int x_position;		// x coordinate of cell position
    int y_position;		// y coordinate of cell position
    void* next_cell;	// pointer to next cell
} __cell;

// __snake - snake structure
typedef struct {
    int direction;	// snake head direction
    int length;		// snake length
    uint16_t colour;// snake colour
    __cell *head;	// snake head cell
    bool AI;		// a flag indicating whether a computer or a player is playing
} __snake;

// __GAME - gameplay structure
typedef struct {
    int x_food;		// x coordinate of food position
    int y_food;		// y coordinate of food position
    int speed;		// snakes speed (frame refresh)
    bool game_on;	// flag that the game is running
    bool quit;		// flag that the program should close
    int map_type;	// map type (for printing)
    int mode;		// mode of the game
	int winner;		// indicates the winner of the game
} __GAME;


enum who_winner 	{NOBODY, PLAYER_1, PLAYER_2};
enum mode_enum 		{SINGLE, YOU_VS_PC, PC_VS_PC, ONLY_PC};
enum direction_enum {NO_MOVE, UP, RIGHT, LEFT, DOWN, TURN_LEFT = -1, TURN_RIGHT = 1};
enum player_enum 	{USER, PC};
enum speed_enum 	{NORMAL = 500000, HARD = 200000, EXTRA_HARD = 100000};
enum game_board 	{WIDTH_BOARD = 23, HEIGHT_BOARD = 12};


extern __snake* snake1;
extern __snake* snake2;
extern __GAME GAME;

__snake* init_snake(int user_type);
void delete_snake(__snake* deleting_snake);

void move_snake(__snake *snake);
bool check_collision(__snake *snake);
bool check_collision_map(__cell *tile);
bool check_collision_multiplayer(__snake* snake, __snake* enemy);
int get_new_direction(int present_direction, int turn_to);

void spawn_food(__snake *snake);

bool do_single_move(__snake* snake);

bool do_multiplayer_move(__snake* player_1, __snake* player_2);
void select_new_direction_BOT(__snake *snake, __snake *enemy);
bool try_possible_move(__snake *snake, int change_in_direction, __snake *enemy);

void print_map();


#endif
